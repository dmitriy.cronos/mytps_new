// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTPSGameMode.h"
#include "MyTPSPlayerController.h"
#include "MyTPS/Character/MyTPSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTPSGameMode::AMyTPSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTPSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AMyTPSGameMode::PlayerCharacterDead()
{

}




